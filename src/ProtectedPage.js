import React from 'react';
import { useAuth } from './authContext';


export default function ProtectedPage() {
    let auth = useAuth();

    function logout() {
        auth.signout()
        console.log(auth.user)
    }

    return (
        <div>
            logged in: { auth.user}
            <button onClick={logout}>Sign out</button>
        </div>
    )
}
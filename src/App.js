import './App.css';
import PrivateRoute from './PrivateRoute';
import ProtectedPage from './ProtectedPage';
import LoginPage from './LoginPage';
import { AuthProvider } from './authContext';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

function App() {
  return (
    <AuthProvider>
      <Router>
        <Switch>
          <Route path="/login" component={LoginPage} />
          <PrivateRoute path="/protected" component={ProtectedPage} />
          <Route path="/*" component={LoginPage} />
        </Switch>
      </Router>
    </AuthProvider>
  );
}

export default App;

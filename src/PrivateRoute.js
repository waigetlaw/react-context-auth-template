import { useAuth } from "./authContext";
import {
    Route,
    Redirect,
} from "react-router-dom";

export default function PrivateRoute(props) {
    let auth = useAuth();
    console.log("from private", auth.user)
    if (!auth.user) return <Redirect to="/login" />
    return <Route {...props} />;
}
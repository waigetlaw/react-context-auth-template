import React, { useEffect } from 'react';
import { useAuth } from './authContext';
import { useHistory } from 'react-router-dom';

export default function Login() {

    let auth = useAuth();
    let history = useHistory();

    useEffect(() => {
        if (auth.user) {
            history.push("/protected");
        }
    }, [auth.user])

    async function submitLogin() {
        await auth.signin();
    }

    return (
        <div>
            <button onClick={submitLogin} >login</button>
        </div>
    )
}